@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Import
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet" type="text/css"/>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Import Products</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">Import</a>
            </li>
            <li class="active">Import Products</li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="bell" data-loop="true" data-color="#fff" data-hovercolor="#fff" data-size="18"></i> Import Products
                            </h3>
                            <span class="pull-right">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                                </span>
                        </div>
                        <div class="panel-body">
                            <form action="#" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Static</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">Static text</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-text-input">Text</label>
                                    <div class="col-md-6">
                                        <input type="text" id="example-text-input" name="example-text-input" class="form-control" placeholder="Text">
                                        <span class="help-block">This is a help text</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-email">Email</label>
                                    <div class="col-md-6">
                                        <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-password">Password</label>
                                    <div class="col-md-6">
                                        <input type="password" id="example-password" name="example-password" class="form-control" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-disabled">Disabled</label>
                                    <div class="col-md-6">
                                        <input type="text" id="example-disabled" name="example-disabled" class="form-control" placeholder="Disabled" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-textarea-input">Textarea</label>
                                    <div class="col-md-9">
                                        <textarea id="example-textarea-input" name="example-textarea-input" rows="7" class="form-control resize_vertical" placeholder="Description.."></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-select">Select</label>
                                    <div class="col-md-6">
                                        <select id="example-select" name="example-select" class="form-control" size="1">
                                            <option value="0">Please select</option>
                                            <option value="1">Bootstrap</option>
                                            <option value="2">CSS</option>
                                            <option value="3">Javascript</option>
                                            <option value="4">HTML</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-multiple-select">Multiple</label>
                                    <div class="col-md-6">
                                        <select id="example-multiple-select" name="example-multiple-select" class="form-control" size="5" multiple>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                            <option value="4">Option #4</option>
                                            <option value="5">Option #5</option>
                                            <option value="6">Option #6</option>
                                            <option value="7">Option #7</option>
                                            <option value="8">Option #8</option>
                                            <option value="9">Option #9</option>
                                            <option value="10">Option #10</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Radios</label>
                                    <div class="col-md-9">
                                        <div class="radio mar-left5">
                                            <label for="example-radio1">
                                                <input type="radio" class="radio-blue" id="example-radio1" name="example-radios" value="option1"> HTML</label>
                                        </div>
                                        <div class="radio mar-left5">
                                            <label for="example-radio2">
                                                <input type="radio" class="radio-blue" id="example-radio2" name="example-radios" value="option2"> CSS</label>
                                        </div>
                                        <div class="radio mar-left5">
                                            <label for="example-radio3">
                                                <input type="radio" class="radio-blue" id="example-radio3" name="example-radios" value="option3"> Javascript</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Inline Radios</label>
                                    <div class="col-md-9">
                                        <label class="radio-inline " for="example-inline-radio1">
                                            <input type="radio" id="example-inline-radio1" class="radio-blue" name="example-inline-radios" value="option1"> HTML</label>
                                        <label class="radio-inline" for="example-inline-radio2">
                                            <input type="radio" id="example-inline-radio2" class="radio-blue" name="example-inline-radios" value="option2"> CSS</label>
                                        <label class="radio-inline" for="example-inline-radio3">
                                            <input type="radio" id="example-inline-radio3" class="radio-blue" name="example-inline-radios" value="option3"> Javascript</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Checkboxes</label>
                                    <div class="col-md-9">
                                        <div class="checkbox mar-left5">
                                            <label for="example-checkbox1">
                                                <input type="checkbox" id="example-checkbox1" class="square-blue" name="example-checkbox1" value="option1">&nbsp;HTML</label>
                                        </div>
                                        <div class="checkbox mar-left5">
                                            <label for="example-checkbox2">
                                                <input type="checkbox" id="example-checkbox2" class="square-blue" name="example-checkbox2" value="option2"> CSS</label>
                                        </div>
                                        <div class="checkbox mar-left5">
                                            <label for="example-checkbox3">
                                                <input type="checkbox" id="example-checkbox3" class="square-blue" name="example-checkbox3" value="option3"> Javascript</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Inline Checkboxes</label>
                                    <div class="col-md-9">
                                        <label class="checkbox-inline mar-left5" for="example-inline-checkbox1">
                                            <input type="checkbox" id="example-inline-checkbox1" class="square-blue" name="example-inline-checkbox1" value="option1"> HTML</label>
                                        <label class="checkbox-inline mar-left5" for="example-inline-checkbox2">
                                            <input type="checkbox" id="example-inline-checkbox2" class="square-blue" name="example-inline-checkbox2" value="option2"> CSS</label>
                                        <label class="checkbox-inline mar-left5" for="example-inline-checkbox3">
                                            <input type="checkbox" id="example-inline-checkbox3" class="square-blue" name="example-inline-checkbox3" value="option3"> Javascript</label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label" for="example-file-input">File</label>
                                    <div class="col-md-9 mar-top">
                                        <input type="file" id="example-file-input" name="example-file-input">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-file-multiple-input">Multiple File</label>
                                    <div class="col-md-9 mar-top">
                                        <input type="file" id="example-file-multiple-input" name="example-file-multiple-input" multiple>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-effect-ripple btn-default">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_layouts.js') }}" type="text/javascript"></script>

@stop
